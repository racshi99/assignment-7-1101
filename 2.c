#include <stdio.h>
int main() {
    char arr[1000], character;
    int count = 0;

    printf("Enter a string: ");
    gets(arr);

    printf("Enter a character to find its frequency: ");
    scanf("%c", &character);

    for (int x = 0; arr[x] != '\0'; ++x) {
        if (character == arr[x])
            ++count;
    }

    printf("Frequency of %c = %d", character, count);
    return 0;
}
